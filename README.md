# M1IF10 Projet Transversal 2019-2020

Cette vise à développer les capacités de travail collaboratives, tout en mettant en pratique les connaissance acquises dans les autres UE du M1.

## Organisation

L'UE est consiste à réaliser un projet en groupe de 6 personnes.

Elle se déroule pendant les périodes creuses. Nous avons bloqué des créneaux et réservé des salles, mais vous êtes libre dans votre organisation.

Le temps de travail attendu par étudiant est de 60h réparties au fil des semaines.

Les discussions se feront de manière publique sur Framavox/Loomio : https://framavox.org/g/0KHbWiCf/lyon1-mif10-2019


[Calendrier de l'UE (google cal).](https://calendar.google.com/calendar/embed?src=4cq51ctpn99qipor3h1o3uije8%40group.calendar.google.com&ctz=Europe%2FBerlin)

## Projet


Thèmes possibles (à spécifier au sein de votre groupe) :

  - Outils de quizz permettant à l'enseignant de poser des questions en cours
    - Choix multiples
    - Votes
    - etc.
  - Poser des questions en temps réel (avec système de vote)
    - en cours 
    - en TP
  - Vérifier la présence des alternants
  - Outil d’évaluation des cours par les élèves 
  - Outil d’évaluation des TPs par les élèves 
  - Outil mobile pour permettre aux enseignants suivre les questions entre plusieurs salles de TP
  - Outil de suivi pour voir ou est ce qu'une classe en est de l'avancement d'un TP (dashboard)
  - Outils pour l'organisation interne des élèves




## Étapes

Semaine 40 (30/09)
- Lancement du projet, voir J1 pour les attendus
- Une permanence de 2h mercredi 2/10

Semaine 43 (21/10)
- Mise en place et démarrage technique voir J2 pour les attendus

Semaine 44 (28/10)
- Développement et tests

Semaine 47 (18/11)
- Développement et tests

Semaine 48 (25/11)
- Finalisation (pas d’ajout de fonctionnalités) 
- Refactoring au besoin
- Correction de tests

Rendu et soutenances ? 
  - Rendu code le 26/11, Soutenance et démo le 28/11 - 29/11
  1. ~~28/11 - 29/11~~
  2. ~~19/12 - 20/12~~
  3. ~~Rendu code le 31/11, Soutenance et démo le 19/12 - 20/12~~


## Jalons et rendus intermédiaires

J1: Besoins et organisation
  - Objectifs et cible utilisateur définie
  - Modalité de travail en groupe
    - Rôles dans l’équipe
    - Outils de travail (communication,redaction de document, ...)
    - Organisation interne (réunions, process, ...)
  - Choix techniques
    - Outils et frameworks/bibliothèques utilisées par le groupe

J2: Proto 1 “[MVP](https://en.wikipedia.org/wiki/Minimum_viable_product)”
  - Cas d’utilisations (ensemble de user story)
  - 1e jet d’architecture
  - Chaîne d’outillage fonctionnelle (Intégration continue : construction, tests, qualité)
  - UNE fonctionnalité de base est implémentée et intégrée. 

J3: Proto 2
  - Tests fonctionnels 
  - 1e tests utilisateurs.

J4: Livraison et présentation
  - Documentation
  - Livraison du code
  - Démo
  - Présentation


Les comptes rendus de réunion doivent lister présents, absents excusés et absents (uniquement pour les membres du projets censés participer à la réunion)
Ces livrables sont à déposer dans le wiki du projet forge, et listés dans une page Wiki "Fichiers"

Compte-rendus de gestion de projet à la fin de chaque mêlée (1 semaine)
  - Résumé d'avancement
  - Points de blocage technique et/ou humains
  - État de la qualité
  - Screenshots Sonarqube


## Évaluation

Le projet sera évalué en trois pans:

  - Rendus intermédiaires
  - [Présentation et démo](eval-enseignants.md)
  - [Évaluation entre pairs](eval-pairs.md)


## Rendu Final

- Le rendu du code est pour le mardi **26/11 23h59**
- Le wiki peut être mis à jour jusqu'au jeudi **28/11 12h00**
- L'évaluation entre pair est à réaliser avant le **29/11 23h59**

- Mettre les intervenants de l'UE comme Reporter de votre projet
- Créer une branche FINAL correspondant au code de la démo
  - Qui datera au plus tard du jour du rendu le **26/11**
- README.md détaillant les dépendances, la procédure de build, et le lien vers une VM de démp
- Un wiki structuré qui contient les rendus intermédiaires et les notes de réunions.
- Le wiki décrira aussi le processus de gestion qualité
  - Tests, et leur gestion (qui écrit, qui teste, etc.)
  - Description des processus automatisés (mis en place ou essayé)
  - Rapports des tests utilisateurs
  - Captures intermédiaires de la qualité (avec Sonarqube)
- Le wiki aura une page dédié à la démo : 
  - Lien vers la VM de démo
  - Instructions d'utilisations
  

## Démonstration et Présentation 

### Déroulé des soutenances 

Les soutenances durent 25 minutes elles sont divisées en 
- 15 minutes de présentation et démo
- 10 minutes de questions

La durée de présentation est stricte (15 minutes), vous serez interrompu si vous dépassez.

#### Conseils de présentation

L'objectif de la présentation est de montrer votre réalisation (sous forme de démo), vos choix techniques, et votre organisation de groupe.

** Démonstration **
  - Préparer un scénario de démonstration, et déroulez le.
  - Mettre en avant les points forts de la réalisation aussi bien technique, qu'en terme d'usage.
  - Discuter des compromis que vous avez du faire, et de vos choix finaux en termes de fonctionalités, d'interface, de sécurité, etc.

** Présentation **
  - Présenter votre mode de fonctionnement collaboratif
  - Présenter vos choix d'architecture, quelques diagrammes UML, les patterns utilisés, etc.
  - Vos méthodes et outils de suivi et de déploiement du code
  - Présenter les résultats de votre [rétrospective Agile](https://www.nutcache.com/fr/blog/learning-matrix-iteration-agile/) (Les points positifs, Les points à améliorer, Les « merci ! », Les idées d’amélioration)
  - Quelles sont les leçons que vous retenez du projet ? 




### Passage des groupes:
Les soutenances sont ouvertes à tous.

*Nous vous demandons d'être présent 15 minutes avant*


#### Jeudi 28/11
Jury: Lionel Médini, Aurélien Tabard
 - 13:00 - 13:30 : Groupe 15
 - 13:30 - 14:00 : Groupe 20
 - 14:00 - 14:30 : Groupe 6
 - 14:30 - 15:00 : Groupe 19
 - pause
 - 15:15 - 15:45 : Groupe 10
 - 15:45 - 16:15 : Groupe 3
 - 16:15 - 16:45 : Groupe 18
 - pause
 - 17:00 - 17:30 : Groupe 21
 - 17:30 - 18:00 : Groupe 13
 - 18:00 - 18:30 : Groupe 16

#### Vendredi 29/11
Jury: Matthieu Moy, Emmanuel Coquery

 - 8:15 - 8:45 : Groupe 7
 - 8:45 - 9:15 : Groupe 11
 - 9:15 - 9:45 : Groupe 2
 - pause
- 10:00 - 10:30 : Groupe 1
- 10:30 - 11:00 : Groupe 17
- 11:00 - 11:30 : Groupe 23
- pause
- 12:30 - 13:00 : Groupe 8
- 13:00 - 13:30 : Groupe 22
- 13:30 - 14:00 : Groupe 4

