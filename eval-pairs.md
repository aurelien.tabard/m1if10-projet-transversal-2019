# Critères d'évaluations au sein du groupe

Chaque membre de l'équipe évaluera les autres membres du groupe sur les critères suivants (note de 1 à 4)

## Table d'évaluation
| Critères d'évaluation                                            | Membre 1 | Membre 2 | Membre 3 | ...      |
|------------------------------------------------------------------|----------|----------|----------|----------|
| Participe aux réunions de groupe régulièrement et ponctuellement |          |          |          |          |
| Contribue de manière constructive aux réunions de groupe         |          |          |          |          |
| Termine ses tâches en temps et en heure                          |          |          |          |          |
| Fournit un travail de qualité                                    |          |          |          |          |
| Démontre une attitude inclusive, coopérative et de soutien à l'équipe |          |          |          |          |
| Contribue au succès du projet                                    |          |          |          |          |



## Retours à faire sur la dynamique de groupe:

1. Dans quelle mesure votre groupe a travaillé de manière efficace.

2. Est ce que certains comportement de membres de l'équipe ont été particulièrement bénéfique, négatifs pour l'équipe ? Expliquer.

3. Qu'avez vous appris sur le travail de groupe lors de ce projet qui vous servira dans les projets à venir ?






## Rappel sur les compétences sociale attendues pour le travail en groupe:

On dispose de compétences sociales quand :

1. on écoute et prend les autres en considération,
2. on prend des initiatives,
3. on sait aussi bien se mettre en avant que se mettre en retrait,
4. on construit et entretient des relations,
5. on distribue et coordonne le travail, 
6. on traite les problèmes de façon à y apporter une solution,
7. on n’abandonne pas à la moindre difficulté,
8. on est prêt à prendre la responsabilité des autres,
9. on peut traiter plusieurs opinions,
10. on intègre des notions de devoirs comme la ponctualité et la fiabilité.